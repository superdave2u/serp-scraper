var Scraper = require('google-scraper')
var csvWriter = require('csv-write-stream')

var fs = require('graceful-fs')
var logStream = fs.createWriteStream('log.txt', {
    'flags': 'a'
})

var log = function (msg) {
    var timestamp = Math.floor(Date.now() / 1000)
    console.log(timestamp + ": " + msg)
    logStream.write(timestamp + ": " + msg + '\n')
}

get_serp_array = function (cb, keyword, results, lang, tld) {
    var kw_str = (typeof keyword == 'undefined') ? '' : keyword
    var lang_str = (typeof lang == 'undefined') ? 'en' : lang
    var tld_str = (typeof tld == 'undefined') ? 'com' : tld
    var results_ct = (typeof results == 'undefined') ? 30 : results

    var options = {
        keyword: kw_str,
        language: lang_str,
        tld: tld_str,
        results: results_ct
    }

    var scrape = new Scraper.GoogleScraper(options)
    scrape.getGoogleLinks.then(function (results) {
        cb(null, results)
    }).catch(function (e) {
        cb(e)
    })
}

array_to_idx_arr = function (cb, array_in) {
    var i = 1
    var array_out = array_in.map(function (el) {
        return ({
            pos: i++,
            url: el
        })
    })
    cb(null, array_out)
}

obj_to_csv = function (cb, idx_arr_in, filename) {

    var date_str = new Date().toISOString().replace(/T.+/, '-')
    var output_file = date_str + kw_str.replace(/ /, '-') + '-results' + '.csv'
    var writer = csvWriter()
    writer.pipe(fs.createWriteStream(output_file))
    idx_arr_in.map(function (this_line) {
        writer.write(this_line)
    })
    writer.end()
    cb(null, output_file)
}

query_serps('"Jeff Coursey" ~pilot -acp -doctor -Texas -Wijayawardana -gulliver -Dean', 100)
